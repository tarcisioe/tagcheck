from typing import Dict, List


class File:
    path: str
    tags: Dict[str, str]
    unsupported: List[str]

    def __init__(self, path):
        ...

    def save() -> None:
        ...

    def removeUnsupportedProperties(self, p: List[str]) -> None:
        ...
