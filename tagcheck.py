import re
import sys

from contextlib import contextmanager
from pathlib import Path
from typing import (
    Callable, Generator, List, NamedTuple, Union, Protocol, cast, no_type_check
)

import taglib

from carl import command, Arg


class Logger(Protocol):
    @staticmethod
    def message(
        path: Path,
        message: str,
        *,
        file=sys.stdout
    ):
        ...

    @classmethod
    def messages(
        cls,
        path: Path,
        messages: List[str],
        *,
        file=sys.stdout
    ):
        ...


class QuietLogger:
    @staticmethod
    def message(
        path: Path,
        message: str,
        *,
        file=sys.stdout
    ):
        pass

    @classmethod
    def messages(
        cls,
        path: Path,
        messages: List[str],
        *,
        file=sys.stdout
    ):
        pass


class NormalLogger:
    @staticmethod
    def message(
        path: Path,
        message: str,
        *,
        output=sys.stdout
    ):
        print(f'{path}: {message}', file=output)

    @classmethod
    def messages(
        cls,
        path: Path,
        messages: List[str],
        *,
        output=sys.stdout
    ):
        for message in messages:
            cls.message(path, message, output=output)


class ReadOnlyFileError(Exception):
    pass


class WriteableSong(taglib.File):
    pass


class ReadOnlySong(taglib.File):
    @staticmethod
    def save():
        raise ReadOnlyFileError('Cannot write to read-only song.')


@contextmanager
def saving(target: Path) -> Generator[WriteableSong, None, None]:
    f = WriteableSong(str(target))
    yield f
    f.save()


@contextmanager
def readonly(target: Path) -> Generator[ReadOnlySong, None, None]:
    f = ReadOnlySong(str(target))
    yield f


Checker = Callable[[ReadOnlySong], List[str]]
Fixer = Callable[[WriteableSong], List[str]]
CheckerFixer = Callable[[Union[ReadOnlySong, WriteableSong]], List[str]]


class TagScannerData(NamedTuple):
    checkers: List[Checker]
    fixers: List[Fixer]
    logger: Logger


class TagScanner(TagScannerData):
    def scan_song(self, f: Path):
        try:
            with readonly(f) as read_only:
                for checker in self.checkers:
                    self.logger.messages(f, checker(read_only))

            if self.fixers:
                with saving(f) as writeable:
                    for fixer in self.fixers:
                        self.logger.messages(f, fixer(writeable))
        except OSError:
            self.logger.message(f, 'Failed to open.')

    def scan_directory(self, dirpath: Path):
        paths = dirpath.glob('**/*')

        files = (p for p in paths if not p.is_dir())

        for f in sorted(files):
            self.scan_song(f)

    def scan(self, path: Path):
        if path.is_dir():
            self.scan_directory(path)
            return

        self.scan_song(path)


def has_unsupported(song: ReadOnlySong) -> List[str]:
    if song.unsupported:
        return [
            'Unsupported tags found.',
            f'{song.unsupported}',
        ]

    return []


def no_tags(song: ReadOnlySong) -> List[str]:
    if not song.tags:
        return ['No tags found.']

    return []


def no_basic_tags(song: ReadOnlySong) -> List[str]:
    tags = [
        ('ALBUM', 'Has no album.'),
        ('ARTIST', 'Has no artist.'),
        ('DATE', 'Has no date.'),
        ('GENRE', 'Has no genre.'),
        ('TITLE', 'Has no title.'),
    ]

    return [message for tag, message in tags if tag not in song.tags]


PUNCTUATION = r'[-()[\]{}&.,;:]'
LOOKBEHIND = fr'(?!^)(?<!{PUNCTUATION})\s+'
LOOKAHEAD = fr'(?!$|{PUNCTUATION})'

WORDS = '|'.join([
    'A', 'An',
    'The',
    'And',
    'But',
    'For', 'To',
    'At',
    'By', 'From', 'Of',
    'In', 'On',
])

CAPITALS = re.compile(fr'{LOOKBEHIND}\b({WORDS})\b{LOOKAHEAD}')


def correct_capitalisation(
    song: Union[ReadOnlySong, WriteableSong]
) -> List[str]:
    title: str
    try:
        title, = song.tags['TITLE']
    except KeyError:
        return []

    capitals = CAPITALS.findall(title)

    if capitals:
        new_title = CAPITALS.sub(lambda m: m.group(0).lower(), title)

        if isinstance(song, WriteableSong):
            song.tags['TITLE'] = new_title
            return [f'Song was poorly capitalized. Renamed to {new_title}']
        return [f'Song is poorly capitalized. Should be {new_title}.']

    return []


def correct_track_number(song: ReadOnlySong) -> List[str]:
    try:
        track_number, = song.tags['TRACKNUMBER']
    except KeyError:
        return ['Song has no track number.']

    try:
        _, _ = track_number.split('/')
    except ValueError:
        return ['Song lacks either number or total.']

    return []


def remove_unsupported(song: WriteableSong) -> List[str]:
    if song.unsupported:
        messages = [
            f'Removing unsupported tag {tag}' for tag in song.unsupported
        ]
        song.removeUnsupportedProperties(song.unsupported)
        return messages

    return []


def has_comments(
    song: Union[ReadOnlySong, WriteableSong]
) -> List[str]:
    messages = []

    comment_tags = [tag for tag in song.tags if tag.startswith('COMMENT')]

    for tag in comment_tags:
        if isinstance(song, WriteableSong):
            del song.tags[tag]
            messages.append(f'Removed comment tag {tag}.')
        else:
            messages.append(f'Has comment tag {tag}.')

    return messages


def complete_date(song: ReadOnlySong) -> List[str]:
    if 'DATE' not in song.tags:
        return []

    if re.match(r'\d{4}-\d{2}-\d{2}', song.tags['DATE'][0]):
        return [f'Has nice date.']
    return []


def tagger(
    target: str,
    print_unsup: bool,
    remove_unsup: bool,
    quiet: bool,
    fix: bool,
    comments_as_errors: bool,
):
    logger = (QuietLogger if quiet else NormalLogger)()

    have_fixer: List[CheckerFixer] = [checker for flag, checker in [
        (True, correct_capitalisation),
        (comments_as_errors, has_comments),
    ] if flag]

    checkers: List[Checker] = [checker for flag, checker in [
        (print_unsup, has_unsupported),
        (True, no_basic_tags),
        (True, correct_track_number),
        (True, complete_date),
    ] if flag]

    fixers: List[Fixer] = [fixer for flag, fixer in [
        (remove_unsup, remove_unsupported),
    ] if flag]

    if fix:
        fixers += [cast(Fixer, x) for x in have_fixer]
    else:
        checkers += [cast(Checker, x) for x in have_fixer]

    scanner = TagScanner(checkers, fixers, logger)

    scanner.scan(Path(target).resolve())


@command
@no_type_check
def main(target: 'File or directory to scan.',
         print_unsup: Arg('-p', '--print-unsupported',
                          help='Print unsupported tags.',
                          action='store_true')=False,
         remove_unsup: Arg('-r', '--remove-unsupported',
                           help='Remove unsupported tags.',
                           action='store_true')=False,
         fix: Arg('-f', '--fix',
                  help='Run fixers instead of just checking.',
                  action='store_true')=False,
         quiet: Arg('-q', '--quiet',
                    help='Do not produce text output.',
                    action='store_true')=False,
         comments_as_errors: Arg('-c', '--comments-as-errors',
                             help='Treat comment tags as errors.',
                             action='store_true')=False,

         ):
    tagger(target, print_unsup, remove_unsup, quiet, fix, comments_as_errors)


def run():
    main.run()


if __name__ == '__main__':
    run()
